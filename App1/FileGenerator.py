def create_file():
    content = """This is a large hardcoded content.
    It can span multiple lines.
    You can include any text or data you want.

    Feel free to customize it as needed.
    """
    file_name = input("file1.txt")

    try:
        with open(file_name, 'w') as file:
            file.write(content)
        print("File created successfully!")
    except IOError as e:
        print(f"Error creating the file: {e}")

if __name__ == "__main__":
    create_file()